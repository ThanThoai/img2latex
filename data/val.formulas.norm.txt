M C = 2 M B
D = \left ( - \frac { 1 2 } { 5 } ; \frac { 3 6 } { 5 } \right )
x \in [ 0 ; 2 ]
x > 0
P = \frac { a ^ { 2 } } { c \left ( c ^ { 2 } + a ^ { 2 } \right ) } + \frac { b ^ { 2 } } { a \left ( a ^ { 2 } + b ^ { 2 } \right ) } + \frac { c ^ { 2 } } { b \left ( b ^ { 2 } + c ^ { 2 } \right ) } + 2 \left ( a ^ { 2 } + b ^ { 2 } + c ^ { 2 } \right )
C _ { 6 } ^ { 0 } ( x - 1 ) ^ { 6 }
\mathrm { H K } \Rightarrow K ( 3 ; 3 )
S B / / O M \Rightarrow S B / / ( M A C ) \Rightarrow d ( S B , C M ) = d ( S B , ( M A C ) ) = d ( S , ( M A C ) ) = d ( D , ( M A C ) )
y = x ^ { 3 } - 3 x ^ { 2 } + 2
I \left ( \frac { 2 2 } { 5 } ; \frac { 1 1 } { 5 } \right )
A ( 2 ; 3 ; - 4 ) , B ( 5 ; 3 ; - 1 )
C G = \frac { A C ^ { 2 } } { S C } = \frac { 2 } { \sqrt { 3 } } a \Rightarrow O H = \frac { \sqrt { 3 } } { 3 } a
z + \bar { z } = 2
\left [ \frac { 1 } { 4 } ; 2 \right ]
y = 2 \cos ^ { 3 } x - \frac { 9 } { 2 } \cos ^ { 2 } x + 3 \cos x + \frac { 1 } { 2 }
\Rightarrow P = \frac { 1 } { \sqrt { a + b c } } + \frac { 1 } { \sqrt { b + c a } } + \frac { 1 } { \sqrt { c + a b } } \geq \frac { 9 } { \sqrt { a + b c } + \sqrt { b + c a } + \sqrt { c + a b } }
n ( \Omega ) = \left ( \mathrm { C } _ { 1 0 } ^ { 3 } \right ) ^ { 2 }
B C = 2 A B , \quad M ( 0 ; 4 )
P = \frac { 1 } { ( a + 1 ) ^ { 2 } } + \frac { 4 } { ( b + 2 ) ^ { 2 } } + \frac { 8 } { ( c + 3 ) ^ { 2 } }
t = \log _ { 2 } x
\Leftrightarrow 2 \sin x ( \cos x - 3 ) + 2 \sin ^ { 2 } x = 0
\Leftrightarrow 3 \sqrt { 2 + x } + 4 \sqrt { 2 + x } \sqrt { 2 - x } = 1 0 - 3 x + 6 \sqrt { 2 - x }
\Rightarrow P ( B ) = \frac { 1 2 } { 1 2 0 } = 0
A C \perp ( S N H ) \Rightarrow A C \perp S H
\frac { 1 3 5 } { 1 6 5 } = \frac { 9 } { 1 1 }
x \in R
\sqrt { 1 7 }
A D = 2 \sqrt { 3 } a
( 2 + \mathrm { i } ) \mathrm { z } + \frac { 1 - \mathrm { i } } { 1 + \mathrm { i } } = 5 - \mathrm { i }
\log _ { 2 } ^ { 2 } x - \left ( \log _ { 4 } 4 + \log _ { 4 } x ^ { 2 } \right ) - 5 = 0 \Leftrightarrow \log _ { 2 } ^ { 2 } x - \log _ { 2 } x - 6 = 0
\left \{ \begin {array} { l } { x ^ { 3 } - y ^ { 3 } + 3 y ^ { 2 } + 3 2 x = 9 x ^ { 2 } + 8 y + 3 6 } \\ { 4 \sqrt { x + 2 } + \sqrt { 1 6 - 3 y } = x ^ { 2 } + 8 } \end {array} \quad ( x , y \in \mathbb { R } ) \right .
\log _ { 2 } x + \log _ { 3 } x + \log _ { 6 } x = \log _ { 3 6 } x
S _ { \Delta A B M } = \frac { 1 } { 2 } A B \cdot d _ { ( M , \Delta ) }
x - y + 1 \leq 0
I ( 3 ; 6 ; 7 )
2 C _ { n } ^ { 1 } - C _ { n } ^ { 2 } + n = 0
A = \frac { 1 } { \sqrt { a ^ { 2 } + 2 b ^ { 2 } } } + \frac { 1 } { \sqrt { b ^ { 2 } + 2 c ^ { 2 } } } + \frac { 1 } { \sqrt { c ^ { 2 } + 2 a ^ { 2 } } }
\mathrm { D } = \mathbb { R }
\lim _ { x \rightarrow ( - 2 ) ^ { + } } y = + \infty ; \lim _ { x \rightarrow ( - 2 ) ^ { - } } = - \infty
\left \{ \begin {array} { l } { 3 \sqrt { y ^ { 3 } ( 2 x - y ) } + \sqrt { x ^ { 2 } \left ( 5 y ^ { 2 } - 4 x ^ { 2 } \right ) } = 4 y ^ { 2 } } \\ { \sqrt { 2 - x } + \sqrt { y + 1 } + 2 = x + y ^ { 2 } } \end {array} \quad ( x , y \in \mathbb { R } ) \right .
I = \int _ { 1 } ^ { e } \frac { \ln ^ { 2 } x } { x ( 1 + 2 \ln x ) } d x
\log _ { 3 } \left ( x ^ { 2 } + 2 x \right ) + \log _ { \frac { 1 } { 3 } } ( 3 x + 2 ) = 0
A ( - 1 ; 2 ; - 1 )
A ( 1 ; 3 ; 5 )
c = 2 \Rightarrow a = b = \frac { 1 } { \sqrt { 2 } } \Rightarrow z = \sqrt { 2 } y = 2 x
\left \{ \begin {array} { l } { ( 1 - y ) \sqrt { x ^ { 2 } + 2 y ^ { 2 } } = x + 2 y + 3 x y } \\ { \sqrt { y + 1 } + \sqrt { x ^ { 2 } + 2 y ^ { 2 } } = 2 y - x } \end {array} ( x , y \in \mathbb { R } ) \right .
A ( 1 ; 1 ; 1 )
y = x ^ { 3 } - 3 m x ^ { 2 } + 2 \left ( C _ { m } \right )
x = - 1
6 + x + 2 \sqrt { ( 1 - x ) ( 2 x - 2 ) } - 1 ( \sqrt { 1 - x } + \sqrt { 2 x - 2 } ) = m
P = 1 2
( 2 ) \Leftrightarrow ( \sqrt { 2 - x } ) ^ { 3 } + 3 \sqrt { 2 - x } = \left ( \frac { 2 } { y } \right ) ^ { 3 } + 3 \left ( \frac { 2 } { y } \right )
A B = A C = a
x ^ { 4 } + 8 x ^ { 2 } + 1 6 m x + 1 6 m ^ { 2 } + 3 2 m + 1 6 = 0
f ^ { \prime } ( c ) = 2 c - \frac { 1 6 } { c ^ { 2 } } \Rightarrow f ^ { \prime } ( c ) = 0 \Leftrightarrow c = 2
f ( x ) = \left ( \sqrt [ 3 ] { x } + \frac { 2 } { \sqrt { x } } \right ) ^ { 1 5 }
\overrightarrow { A B } = ( - 1 ; 1 ; - 1 )
I ( 3 ; - 1 ) , K ( 2 ; 2 )
\widehat { A B C } = 6 0 ^ { 0 }
\sqrt { 2 1 }
K \left ( \frac { 4 1 } { 2 6 } ; \frac { 1 0 1 } { 2 6 } \right )
x \in \left [ 0 ; \frac { \pi } { 2 } \right ] : A = 3 \sin x ; 2 B = 3 \cos x
z ( 1 - 2 i ) + \bar { z } = 1 0 - 4 i
t = - \frac { 5 } { 2 2 } \Rightarrow M \left ( - \frac { 1 5 } { 2 2 } ; - \frac { 6 } { 1 1 } ; - \frac { 7 } { 2 2 } \right )
y = x ^ { 3 } - 3 x ^ { 2 } - 1
I = \int _ { 0 } ^ { 1 } \frac { \left ( x ^ { 2 } + x \right ) e ^ { x } } { x + e ^ { - x } } d x
A H \perp B C
I = \int _ { 1 } ^ { 2 } \frac { 1 + x ^ { 2 } e ^ { x } } { x } d x
\mathrm { E } = \mathrm { B N } \cap \mathrm { A D } \Rightarrow \mathrm { D }
J = - \frac { 1 } { 2 } \ln 2 - \left . \frac { 1 } { x } \right | _ { 1 } ^ { 2 } = - \frac { 1 } { 2 } \ln 2 + \frac { 1 } { 2 }
( \mathrm { S } ) : \mathrm { x } ^ { 2 } + \mathrm { y } ^ { 2 } + \mathrm { z } ^ { 2 } - 2 \mathrm { x } - 4 \mathrm { y } - 6 \mathrm { z } - 1 1 = 0
\cos 2 x + ( 1 + 2 \cos x ) ( \sin x - \cos x ) = 0 \Leftrightarrow ( \sin x - \cos x ) ( \cos x - \sin x + 1 ) = 0
n ( \Omega ) = C _ { 1 1 } ^ { 3 } = 1 6 5
f ( c ) \geq f ( 2 ) = 1 2
\mathrm { d } \left ( \mathrm { B } ; \left ( \mathrm { A B } ^ { \prime } \mathrm { C } ^ { \prime } \right ) \right ) = \frac { a \sqrt { 3 } } { 4 }
a ^ { 2 } + b ^ { 2 } + c ^ { 2 } - 3 b \leq 0
D H \perp A B \text { v } D H = a \sqrt { 3 } ; \quad O K / / D H
\Leftrightarrow m \geq \frac { x ^ { 2 } - 4 x + 1 } { x + 1 }
\frac { 1 } { \mathrm { A H } ^ { 2 } } = \frac { 1 } { \mathrm { A B } ^ { 2 } } + \frac { 1 } { \mathrm { A E } ^ { 2 } } = \frac { 5 } { 4 \mathrm { A B } ^ { 2 } }
\Rightarrow O H = \frac { 1 } { 2 } C G
\frac { 1 - \cos 2 x } { \sin ^ { 2 } 2 x } - \cot 2 x = 1
A = \frac { 1 } { 3 } x e ^ { 3 x } - \frac { 1 } { 9 } e ^ { 3 x } + \frac { 1 } { 2 } \ln \left | x ^ { 2 } + 1 \right | + C
A B = a , \widehat { A C B } = 3 0 ^ { 0 }
\log _ { 3 } ^ { 2 } x + 4 \log _ { 9 } ( 9 x ) - 7 = 0
y = - x + 2 ( d )
\Delta
\mathrm { B } ( - 4 ; - 2 )
\log _ { 2 } ^ { 2 } x - 4 \log _ { 4 } x ^ { 3 } + 5 = 0
d : x + 2 y = 0 \quad \Delta : 4 x + 3 y = 0
A ( - 1 ; 2 )
\Leftrightarrow ( 1 + 2 t ) ^ { 2 } + ( 4 + 2 t ) ^ { 2 } + ( 1 - t ) ^ { 2 } = 4 5
x - y = 0 , \mathrm { E }
I = \int _ { 1 } ^ { e } \frac { \left ( x ^ { 2 } + 1 \right ) \ln x + 4 x + 1 } { 4 + x \ln x } d x
y = \frac { 1 } { 3 } x ^ { 3 } - x ^ { 2 }
y = \left ( - 6 x _ { 0 } ^ { 2 } + 1 2 x _ { 0 } \right ) \left ( x - x _ { 0 } \right ) - 2 x _ { 0 } ^ { 3 } + 6 x _ { 0 } ^ { 2 } - 5
\overrightarrow { A G } = \frac { 2 } { 3 } \overrightarrow { A M } \Leftrightarrow \left \{ \begin {array} { l } { x _ { G } = \frac { 2 } { 3 } \cdot \frac { 5 c + 5 } { 7 } } \\ { y _ { G } = - \frac { 2 } { 3 } \cdot \frac { c + 1 } { 7 } } \end {array} \Leftrightarrow \left \{ \begin {array} { l } { x _ { G } = \frac { 1 0 c + 1 0 } { 2 1 } } \\ { y _ { G } = \frac { - 2 c - 2 } { 2 1 } } \end {array} \right . \right .

